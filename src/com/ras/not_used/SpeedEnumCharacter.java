package com.ras.not_used;

/**
 * Created by ras on 31.01.2018.
 */
public enum SpeedEnumCharacter {
    VALUE(true),
    UNIT(false),
    VALUE_IN_MS(true);

    private boolean ascend;

    private SpeedEnumCharacter(boolean ascend) {
        this.ascend = ascend;
    }
    public void setAscend(boolean ascend) {
        this.ascend = ascend;
    }
    public boolean getAscend() {
        return ascend;
    }
}
