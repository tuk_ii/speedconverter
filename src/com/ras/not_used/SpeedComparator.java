package com.ras.not_used;

import com.ras.speed_converter_package.SpeedCharacteristic;

import java.util.Comparator;

/**
 * Created by ras on 31.01.2018.
 *
 * не нужен
 */
public class SpeedComparator implements Comparator<SpeedCharacteristic> {
    private SpeedEnumCharacter sortingIndex;

    public SpeedComparator(SpeedEnumCharacter sortingIndex) {
        setSortingIndex(sortingIndex);
    }

    public final void setSortingIndex(SpeedEnumCharacter sortingIndex) {
        if (sortingIndex == null) {
            throw new IllegalArgumentException();
        }
        this.sortingIndex = sortingIndex;
    }

    public SpeedEnumCharacter getSortingIndex() {
        return sortingIndex;
    }

    @Override
    public int compare(SpeedCharacteristic speed1, SpeedCharacteristic speed2) {
        switch (sortingIndex) {
            case VALUE:
                return Integer.compare(speed1.getValue(), speed2.getValue());
            case UNIT:
                return speed1.getUnit().compareTo(speed2.getUnit());
            case VALUE_IN_MS:
                return Double.compare(speed1.getValueInMs(), speed2.getValueInMs());
            default:
                throw new EnumConstantNotPresentException(SpeedEnumCharacter.class, sortingIndex.name());
        }
    }
}
