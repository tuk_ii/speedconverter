package com.ras.speed_converter_package;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Created by ras on 30.01.2018.
 */
public class SpeedConverterFunction {

    public static void converterRunner(String fileName) {
        ArrayList<SpeedCharacteristic> sc = new ArrayList<SpeedCharacteristic>();
// заполнение массива
        try (Scanner scan = new Scanner(new FileReader(fileName))) {
            while (scan.hasNext()) {
                sc.add(new SpeedCharacteristic(scan.nextInt(), scan.next()));
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
// форматный вывод в консоль без сортировки
        for (int index = 0; index < sc.size(); index++) {
            System.out.printf(sc.get(index).toString() + "\t=\t%03.3f ms\n", sc.get(index).getValueInMs());
        }
        System.out.print("\n");
// упорядоченный по возрастанию список скоростей, учитывая единицы измерения
        SpeedCharacteristic.ValueInMsComparator valueInMsComparator = new SpeedCharacteristic.ValueInMsComparator();
        Collections.sort(sc, valueInMsComparator);

        for (int index = 0; index < sc.size(); index++) {
            System.out.println(sc.get(index).toString());
        }
        System.out.print("\n");

// определить, есть ли в списке хотя бы одно значение скорости, равное 2 километра в час
        String status = new String("No!");
        for (int index = 0; index < sc.size(); index++) {
            if (sc.get(index).speedEquals()) {
                status = "Yes!";
                break;
            }
        }
        System.out.println(status);
        System.out.print("\n");
// упорядоченный по убыванию список скоростей, сгруппированный по единицам измерения
// (вначале kmh, затем mph, kn и последними ms).

        Comparator<SpeedCharacteristic> speedCharacteristicComparator = new SpeedCharacteristic.SpeedUnitPrioComparator().thenComparing(new SpeedCharacteristic.ValueComparator());
        Collections.sort(sc, speedCharacteristicComparator);

        for (int index = 0; index < sc.size(); index++) {
            System.out.println(sc.get(index).toString());
        }
        System.out.print("\n");

    }

}

