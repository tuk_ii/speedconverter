package com.ras.speed_converter_package;

import java.util.Comparator;

/**
 * Created by ras on 30.01.2018.
 */
public class SpeedCharacteristic {
    private Integer value;
    private String unit;
    private Double value_in_ms;
    private byte speedUnitPrio;

    // конструктор
    public SpeedCharacteristic(Integer value, String unit) {
        this.value = value;
        this.unit = unit;
        this.value_in_ms = parseVelocity(this.value, this.unit);
    }

    // гетеры и сетеры
    public Integer getValue() {
        return value;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public Double getValueInMs() {
        return value_in_ms;
    }

    public Byte getSpeedUnitPrio() {
        return speedUnitPrio;
    }

    public boolean speedEquals() {

        if ((this.value == 2) && (this.unit == "kmh")) {
            return true;
        }
        return false;
    }

    private Double parseVelocity(Integer value, String unit) {
        final double MS_KMH_CONVERTER = 1 / 3.6;
        final double MS_MPH_CONVERTER = 1.609 * MS_KMH_CONVERTER;
        final double MS_KN_CONVERTER = 1.852 * MS_KMH_CONVERTER;

        switch (unit) {
            case "ms": {
                this.speedUnitPrio = 3;
                return (double) value;
            }
            case "kmh": {
                this.setUnit("kmh");
                this.speedUnitPrio = 0;
                return (double) value * MS_KMH_CONVERTER;
            }
            case "mph": {
                this.speedUnitPrio = 1;
                return (double) value * MS_MPH_CONVERTER;
            }
            case "kn": {
                this.speedUnitPrio = 2;
                return (double) value * MS_KN_CONVERTER;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }

    @Override
    public String toString() {
        return (value + " " + unit);
    }

    public static class ValueComparator implements Comparator<SpeedCharacteristic> {
        @Override
        public int compare(SpeedCharacteristic speed1, SpeedCharacteristic speed2) {
            return Integer.compare(speed2.getValue(), speed1.getValue());
        }
    }

    public static class SpeedUnitPrioComparator implements Comparator<SpeedCharacteristic> {

        @Override
        public int compare(SpeedCharacteristic speed1, SpeedCharacteristic speed2) {
            return Byte.compare(speed1.getSpeedUnitPrio(), speed2.getSpeedUnitPrio());
        }
    }

    public static class ValueInMsComparator implements Comparator<SpeedCharacteristic> {
        @Override
        public int compare(SpeedCharacteristic speed1, SpeedCharacteristic speed2) {
            return Double.compare(speed1.getValueInMs(), speed2.getValueInMs());
        }
    }
}
